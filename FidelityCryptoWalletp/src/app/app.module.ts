import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { AppRoutingModule, RoutingComponent } from './app.routing';
import { SidebarComponent } from './sidebar/sidebar.component';
import { SummaryComponent } from './summary/summary.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { ExchangeComponent } from './exchange/exchange.component';
import { SettingsComponent } from './settings/settings.component';
import { WalletComponent } from './wallet/wallet.component';
import { HelpComponent } from './help/help.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegisterComponent } from './register/register.component';

import { HttpClientModule } from '@angular/common/http';
import { APIService} from './services/api.service';
import { LoginService} from './services/login.service';
@NgModule({
  declarations: [
    RoutingComponent,
    AppComponent,
    SidebarComponent,
    SummaryComponent,
    PortfolioComponent,
    ExchangeComponent,
    SettingsComponent,
    WalletComponent,
    HelpComponent,
    RegisterComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ReactiveFormsModule,
    RouterModule,
    HttpClientModule
  ],
  providers: [LoginService, APIService],
  bootstrap: [AppComponent]
})
export class AppModule { }
