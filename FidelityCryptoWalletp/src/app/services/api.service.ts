import { Injectable } from '@angular/core';
import { RequestOptions, Response } from '@angular/http';
import { HttpClient,HttpHeaders  } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
// import { environment } from '../../../environments/environment';

@Injectable()
export class APIService {

    public static getServerUrl(uri: string): string {
        return "sdasd";
    }

    constructor(protected http: HttpClient) {
    }

    get headers(): HttpHeaders{
        return new HttpHeaders({ 'Content-Type': 'application/json' });
    }

    // get requestOptions(): RequestOptions {
    //     const headers = this.headers;
    //     // headers.append('Authorization', 'Bearer ' + this.token);
    //     headers.append('Access-Control-Allow-Origin', '*');

    //     return new RequestOptions({ headers: headers });
    // }

    protected handleErrorObservable(error: Response | any) {
        return Observable.throw(error.message || error);
    }

    protected handleError(response: Response): Promise<any> {
        const isJson = /json/.test(response.headers.get('Content-Type')),
            error = isJson ? response.json() : 'Unknown Error';

        return Promise.reject(error.message || error);
    }

}
