import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Headers, RequestOptions } from '@angular/http';
import { APIService } from './api.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { HttpClient } from 'selenium-webdriver/http';
@Injectable()
export class LoginService extends APIService {

  constructor(http: HttpClient) {
    super(http);
  }

  login(email, password): Observable<any> {
    let httpHeaders = new HttpHeaders()
    .set('Content-Type', 'application/x-www-form-urlencoded'); 
    let url = this.getUrl();
    return this.http.post<any>(url + '?email=' + email + 'password=' + password, {
      headers: httpHeaders,
      responseType: 'json'
    });
  }
  getUrl() {
    return "http://nitu.dhaka/api.php";
  }
}
