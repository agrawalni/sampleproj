import { NgModule } from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';

//Importing afterAll components
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NewsComponent } from './news/news.component';
import { SummaryComponent } from './summary/summary.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { ExchangeComponent } from './exchange/exchange.component';
import { SettingsComponent } from './settings/settings.component';
import { WalletComponent } from './wallet/wallet.component';
import { HelpComponent } from './help/help.component';
import { RegisterComponent } from './register/register.component';

const routes: Routes = [
    { path: 'Login', component: LoginComponent },
    {
        path: 'dashboard', component: DashboardComponent, children: [
            {  path:'',   redirectTo: 'summary',  pathMatch: 'full'},
            { path: 'summary', component: SummaryComponent },
            { path: 'portfolio', component: PortfolioComponent },
            { path: 'exchange', component: ExchangeComponent },
            { path: 'settings', component: SettingsComponent },
            { path: 'wallet', component: WalletComponent },
            { path: 'help', component: HelpComponent },
        ]
    },
    { path: 'news', component: NewsComponent },
    { path: 'register', component: RegisterComponent },
    // To set default route
    { path: '', redirectTo: 'Login', pathMatch: 'full' },
    { path: '**', redirectTo: 'Login' }
    // To set default route
];
@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {

}

export const RoutingComponent = [LoginComponent, DashboardComponent,
    SummaryComponent, ExchangeComponent, SettingsComponent,
    WalletComponent, HelpComponent, NewsComponent,RegisterComponent
];

