import { Component, OnInit } from '@angular/core';
import { Routes, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LoginService } from '../services/login.service';
import { Observable } from 'rxjs/Observable';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  email: FormControl;
  password: FormControl;
  serviceResponse: boolean;
  error: string;
  constructor(private router: Router, private LoginService: LoginService) { }
  ngOnInit() {
    this.email = new FormControl(this.email, Validators.required);
    this.password = new FormControl(this.password, Validators.required);
    this.loginForm = new FormGroup({
      'email': this.email,
      'password': this.password
    });
    if (this.loginForm.valid)
      this.doLogin(this.loginForm);
  }

  doLogin(loginForm) {
    console.log(loginForm);
    this.LoginService.login(loginForm.value['email'], loginForm.value['password'])
      .subscribe(data => {
        this.serviceResponse = data['stat'];
        if (this.serviceResponse == true) {
          this.router.navigate(['dashboard']);
        } else {
          this.errormsg('Invalid login');
        }
      });
  }
  onSubmit() {
    if (this.loginForm.valid) {
      this.doLogin(this.loginForm);
    }
  }
  registerRedirect() {
    console.log('register button');
    this.router.navigate(['register']);
  }
  errormsg(msg) {
    this.error = msg;
  }
}
